"""Utilities to create random Jams objects.
"""

from .. import pyjams

import random
import string

ASCII_SET = string.ascii_letters + ' '*10 + string.digits


def random_string(low=4, high=20):
    return "".join(
        [random.choice(ASCII_SET) for n in xrange(random.randint(low, high))])


def random_Observation(value_type='float'):
    """Populate an Observation with random values.

    Parameters
    ----------
    value_type: str
        Type of value field, one of ['string', 'float']
    """

    if value_type == 'string':
        value = random_string(4, 20)
    elif value_type == 'float':
        value = random.uniform(0, 1.0)

    x = pyjams.Observation(
        value=value,
        confidence=random.uniform(0, 1.0),
        secondary_value=[random.uniform(0, 1.0), None][random.randint(0, 1)])
    return x


def random_Event(max_time=300.0):
    """Populate an Event with random values.
    """
    x = pyjams.Event(
        time=random_Observation('float'),
        label=random_Observation('string'))
    x.time.value = random.uniform(0, max_time)
    return x


def random_Range(max_time=300.0):
    """Populate a Range with random values."""
    x = pyjams.Range(
        start=random_Observation('float'),
        end=random_Observation('float'),
        label=random_Observation('string'))
    times = [random.uniform(0, max_time) for n in range(2)]
    times.sort()
    x.start.value = times[0]
    x.end.value = times[1]
    return x


def random_TimeSeries(min_length=200, max_length=1000):
    """Populate a TimeSeries with random values."""
    N = random.randrange(min_length, max_length)
    time = [random.uniform(0, 1.0) for n in range(N)]
    time.sort()
    x = pyjams.TimeSeries(
        value=[random.uniform(0, 1000) for n in range(N)],
        time=time,
        confidence=[random.uniform(0, 1.0) for n in range(N)])
    return x


def random_AnnotationMetadata():
    """Populate an AnnotationMetadata object with random values."""
    metadata = pyjams.AnnotationMetadata(
        attribute=random_string(),
        corpus=random_string(),
        version=random_string(),
        annotator=pyjams.Annotator(
            name=random_string(),
            email=random_string()),
        annotation_tools=random_string(),
        annotation_rules=random_string(),
        validation_and_reliability=random_string(),
        origin=random_string())
    return metadata


def random_Annotation(num_items):
    return pyjams.Annotation(
        data=[random_Observation() for n in range(num_items)],
        annotation_metadata=random_AnnotationMetadata())


def random_EventAnnotation(num_items, max_time=300):
    return pyjams.EventAnnotation(
        data=[random_Event(max_time=max_time) for n in range(num_items)],
        annotation_metadata=random_AnnotationMetadata())


def random_RangeAnnotation(num_items, max_time=300):
    return pyjams.RangeAnnotation(
        data=[random_Range(max_time=max_time) for n in range(num_items)],
        annotation_metadata=random_AnnotationMetadata())


def random_TimeSeriesAnnotation(num_items, min_length=200, max_length=1000):
    return pyjams.TimeSeriesAnnotation(
        data=[random_TimeSeries(min_length=min_length, max_length=max_length)
              for n in range(num_items)],
        annotation_metadata=random_AnnotationMetadata())


def random_Metadata():
    rand_data = dict([(k, random_string()) for k in pyjams.Metadata().keys()])
    return pyjams.Metadata(**rand_data)
