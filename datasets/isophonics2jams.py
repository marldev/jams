#!/usr/local/bin/python
"""Script to import Isophonics datasets to JAMS.
 - Consistent as of 25 April 2014 -

Sample Usage:
$ ./isophonics2jams.py "~/Queen Annotations/" isophonics-queen ./queen_jams

Note that this script consolidates all files into a single folder, collapsing
nested directories as underscores in a filepath and removing the task name from
the path.

For example, this folder hierarchy:
/Queen Annotations/
    chordlab/
        Queen/
            Greatest Hits I/
                01 Bohemian Rhapsody.lab
                02 Another One Bites The Dust.lab
                ...
    seglab/
        Queen/
            Greatest Hits I/
                01 Bohemian Rhapsody.lab
                02 Another One Bites The Dust.lab
                ...

Will be mapped to:
/queen_jams/
    Queen-Greatest Hits I-01 Bohemian Rhapsody.lab
    Queen-Greatest Hits I-02 Another One Bites The Dust.lab


Partial Isophonics Documentation (copied here for posterity)
- see http://isophonics.net/content/reference-annotations for a current version

Introduction
------------
This web page accompanies a late-breaking paper at the 11th Conference on Music
Information Retrieval [1].

On The Annotation Data
----------------------
The annotations published here fall into four categories: chords, keys,
structural segmentations, and beats/bars. The aim is to allow more musically
driven Music Information Retrieval by combining several features that are
intrinsically linked. Of course, you can also use the annotations of one single
musical feature separately.

We are proud to publish this data, but please be aware that it will never be
exactly "correct", for several reasons:

- music is always ambiguous, and different people will hear different things in
  the same music, so a label can only reflect the perceived truth of a person
  or a group of people,
- the medium we present the annotations in is not perfect, and the syntax and
  more generally the method with which our features are annotated may still be
  improved, and finally
- typos and other errors due to misinterpretations by the annotator (that go
  beyond the difference in perception as noted above) may occur.

The annotations come with "confidence labels" (see below, and in the individual
RDF files), to tell you to which extent we think this last "typo" source of
incorrectness is relevant.

Below you find more specific remarks to the individual annotation types for the
text files.

Chords
------
Comply with Chris Harte's syntax detailed in his 2010 PhD thesis [2] (which
supercedes the 2005 ISMIR proposal paper [3]. The .lab files are whitespace
delimited text files with three columns, corresponding to, onset time, offset
time and chord label, respectively. Our confidence judgement:

- The Beatles: checked several times by Christopher Harte and the MIR
  community, use with confidence.
- Zweieck, Queen: checked by Matthias, use with moderate confidence.
- Carole King: Have not been carefully checked, use with care.

Segmentations
-------------
These are structural segmentations, labelling verses, choruses, refrains etc..
The .lab files are whitespace delimited text files with three columns,
corresponding to, onset time, offset time and segment label, respectively.
Every segment starts at a bar boundary, and the section boundaries should be
quite reliable. Parts (like a verse) that are partly repeated (maybe later in
the song) but otherwise appear as one are usually treated as one, i.e. no
subdivisions. The labelling however is not very consistent so far (note the
"silence" labels), so using the labels to assess whether an algorithm correctly
detects that two segments are "the same" may cause grief. Generally use with
care.

Keys
----
These labels denote tonality regions in a song. The .lab files are whitespace
delimited text files with three columns, corresponding to, onset time, offset
time and key label, respectively. Generally use with care, especially the
Beatles keys: the main key will always be annotated, but key changes may be
omitted in some files.

- Queen, Zweieck, Carole King: checked by Matthias Mauch, use with moderate
confidence.
- The Beatles: often only the main key is given. It may be the only key or it
may not. Use with care.

Beats
-----
The beat files come as whitespace delimited .txt files, which the first column
denotes the beat time, the second column is the metric position. Bar labels can
be derived from that by using only times at which the metric position is "1".

- The Beatles: checked by Matthew Davies. Use with moderate confidence.
- Zweieck: checked by Helena du Toit and Matthias Mauch. Use with moderate confidence.

Audio
-----
We used the audio as provided on the following CDs:

- Carole King: Tapestry, Ode Records, 4931802003
- Queen: Greatest Hits I, Parlophone, 0777 7 8950424
- Queen: Greatest Hits II, Parlophone, CDP 7979712
- Queen: Greatest Hits III, Parlophone, 7243 52389421
- The Beatles: Please Please Me, CDP 7 46435 2
- The Beatles: With the Beatles, CDP 7 46436 2
- The Beatles: A Hard Day’s Night, CDP 7 46437 2
- The Beatles: Beatles For Sale, CDP 7 46438 2
- The Beatles: Help!, CDP 7 46439 2
- The Beatles: Rubber Soul, CDP 7 46440 2
- The Beatles: Revolver, CDP 7 46441 2
- The Beatles: Sgt. Pepper’s Lonely Hearts Club Band, CDP 7 46442 2
- The Beatles: Magical Mystery Tour, CDP 7 48062 2
- The Beatles: The Beatles (the white album), CDS 7 46443 8
- The Beatles: Abbey Road, CDP 7 46446 2
- The Beatles: Let It Be, CDP 7 46447 2
- Zweieck: Zwielicht, contact klavier@zweieck.net for info or to purchase

In the progress of labelling the chords, we used the following literature to
verify our judgements:
- Queen, Greatest Hits I, International Music Publications Ltd, London,
    ISBN 0-571-52828-7
- Queen, Greatest Hits II, Queen Music Ltd./EMI Music Publishing (Barnes Music
    Engraving), ISBN 0-86175-465-4
- Carole King, Tapestry, International Music Publications Ltd,
    London, ISBN 1-84328-452-9
- Additionally, Alan W. Pollack's Notes on ... Series has been a great source
    of information [4].

[1] - http://www.matthiasmauch.net/_bilder/late-breaking-C4DM.pdf
[2] - https://code.soundsoftware.ac.uk/attachments/download/330/chris_harte_phd_thesis.pdf
[3] - href="http://ismir2005.ismir.net/proceedings/1080.pdf
[4] - http://www.icce.rug.nl/~soundscapes/DATABASES/AWP/awp-notes_on.shtml
"""

import argparse
import glob
from os.path import join, splitext, exists
from os import makedirs

from jams.pyjams import JAMS
from jams.pyjams import save
from jams.converters import utils


# Isophonics sub-directory to JAMS task name
SUBDIR_MAP = {
    'beat': 'beat',
    'chordlab': 'chord',
    'keylab': 'key',
    'seglab': 'segment',
}


def collect_lab_files(input_directory):
    files = []
    for subdir in SUBDIR_MAP:
        # Carole King
        files.extend(glob.glob(join(input_directory, "%s/*/*.lab" % subdir)))
        # Everything else
        files.extend(glob.glob(join(input_directory, "%s/*/*/*.lab" % subdir)))
        files.extend(glob.glob(join(input_directory, "%s/*/*/*.txt" % subdir)))
    return files


def isophonics_archive_to_jams(input_directory, corpus_name, output_directory):
    jam_set = dict()
    for lab_file in collect_lab_files(input_directory):
        rel_path = lab_file.split(input_directory)[-1]
        name = splitext("_".join(rel_path.split('/')[1:]))[0]
        task = SUBDIR_MAP[rel_path.split('/')[0]]
        if not name in jam_set:
            jam_set[name] = JAMS()
        annot = jam_set[name][task].create_annotation()
        annot.annotation_metadata.corpus = corpus_name
        if task == 'beat':
            utils.import_event_lab(lab_file, annot)
        else:
            utils.import_range_lab(lab_file, annot)

    for name in jam_set:
        save(jam_set[name], join(output_directory, "%s.jams" % name))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="")

    parser.add_argument("input_directory",
                        metavar="input_directory", type=str,
                        help="")
    parser.add_argument("corpus_name",
                        metavar="corpus_name", type=str,
                        help="")
    parser.add_argument("output_directory",
                        metavar="output_directory", type=str,
                        help="")

    args = parser.parse_args()
    if not exists(args.output_directory):
        makedirs(args.output_directory)

    isophonics_archive_to_jams(
        args.input_directory, args.corpus_name, args.output_directory)
