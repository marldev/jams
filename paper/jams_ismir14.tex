% -----------------------------------------------
% Template for ISMIR 2014
% (based on earlier ISMIR templates)
% -----------------------------------------------

\documentclass{article}
\usepackage{ismir,amsmath,cite}
\usepackage{graphicx}
\usepackage{authblk}
\usepackage{url}
\usepackage{listings}
\usepackage{float}
\usepackage[hang,flushmargin]{footmisc} 
\lstset{
         basicstyle=\footnotesize\ttfamily
}
% \usepackage{minted}
\renewcommand\Authfont{\bfseries}
% Title.
% ------
\title{{JAMS}: {A} {JSON} {A}nnotated {M}usic {S}pecification for
Reproducible {MIR} Research}

% Single address
% To use with only one author or several with the same address
% ---------------

\author[1,*]{Eric J. Humphrey}
\author[1,2]{Justin Salamon}
\author[1]{Oriol Nieto}
\author[1]{Jon Forsyth}
\author[1]{\authorcr Rachel M. Bittner}
\author[1]{Juan P. Bello}
\affil[1]{Music and Audio Research Lab\\New York University, New York}
\affil[2]{Center for Urban Science and Progress\\New York University, New York}

\def\authorname{Eric J. Humphrey, Justin Salamon, Oriol Nieto, Jon Forsyth, Rachel M. Bittner, Juan P. Bello}


\begin{document} 
%
\maketitle
%
\let\oldthefootnote\thefootnote
\renewcommand{\thefootnote}{\fnsymbol{footnote}}
\footnotetext[1]{Please direct correspondence to \url{ejhumphrey@nyu.edu}}
\let\thefootnote\oldthefootnote

\begin{abstract}

The continued growth of MIR is motivating more complex annotation data, consisting of richer information, multiple annotations for a given task, and multiple tasks for a given music signal.
In this work, we propose JAMS, a JSON-based music annotation format capable of addressing the evolving research requirements of the community, based on the three core principles of simplicity, structure and sustainability.
It is designed to support existing data while encouraging the transition to more consistent, comprehensive, well-documented annotations that are poised to be at the crux of future MIR research.
Finally, we provide a formal schema, software tools, and popular datasets in the proposed format to lower barriers to entry, and discuss how now is a crucial time to make a concerted effort toward sustainable annotation standards.

\end{abstract}
%
\section{Introduction}
\label{sec:introduction}

Music annotations ---the collection of observations made by one or more agents about an acoustic music signal--- are an integral component of content-based Music Information Retrieval (MIR) methodology, and are necessary for designing, evaluating, and comparing computational systems.
For clarity, we define the scope of an annotation as corresponding to time scales at or below the level of a complete song, such as semantic descriptors (tags) or time-aligned chords labels.
Traditionally, the community has relied on plain text and custom conventions to serialize this data to a file for the purposes of storage and dissemination, collectively referred to as ``lab-files''.
%An excerpt of a chord lab-file is given in Figure \ref{fig:chord_lab}, for example, where different chords are separated by rows and the columns provide the start time, end time, and label.
Despite a lack of formal standards, lab-files have been, and continue to be, the preferred file format for a variety of MIR tasks, such as beat or onset estimation, chord estimation, or segmentation.

%\begin{figure}[t]
% \centerline{
% \includegraphics[width=3in]{figs/walhfmf_lab.pdf}}
% \caption{Excerpt of a lab-file chord annotation for ``With a Little Help from My Friends'' by The Beatles.}
% \label{fig:chord_lab}
%\end{figure}

Meanwhile, the interests and requirements of the community are continually evolving, thus testing the practical limitations of lab-files.
By our count, there are three unfolding research trends that are demanding more of a given annotation format: %EDIT(rmb): By our count, there are three developing trends that demand a more standardized annotation format:

\begin{itemize}
\item \textbf{Comprehensive annotation data}:
Rich annotations, like the Billboard dataset\cite{burgoyne2011expert}, require new, content-specific conventions, increasing the complexity of the software necessary to decode it and the burden on the researcher to use it; such annotations can be so complex, in fact, it becomes necessary to document how to understand and parse the format\cite{de2012parsing}.

\item \textbf{Multiple annotations for a given task}: 
The experience of music can be highly subjective, at which point the notion of ``ground truth'' becomes tenuous.
Recent work in automatic chord estimation \cite{ni2013understanding} shows that multiple reference annotations should be embraced, as they can provide important insight into system evaluation, as well as into the task itself.

\item \textbf{Multiple concepts for a given signal}: 
Although systems are classically developed to accomplish a single task, there is ongoing discussion toward integrating information across various musical concepts \cite{vincent2010roadmap}.
This has already yielded measurable benefits for the joint estimation of chords and downbeats \cite{papadopoulos2011joint} or chords and segments \cite{mauch2009using}, where leveraging multiple information sources for the same input signal can lead to improved performance.
\end{itemize}


\noindent It has long been acknowledged that lab-files cannot be used to these ends, and various formats and technologies have been previously proposed to alleviate these issues, such as RDF \cite{cannam2006sonic}, HDF5\cite{bertin2011million}, or XML \cite{mckay2005ace}.
However, none of these formats have been widely embraced by the community.
We contend that the weak adoption of any alternative format is due to the combination of several factors.
For example, new tools can be difficult, if not impossible, to integrate into a research workflow because of compatibility issues with a preferred development platform or programming environment. 
Additionally, it is a common criticism that the syntax or data model of these alternative formats is non-obvious, verbose, or otherwise confusing.
This is especially problematic when researchers must handle format conversions.
Taken together, the apparent benefits to conversion are outweighed by the tangible costs.

In this paper, we propose a JSON Annotated Music Specification (JAMS) to meet the changing needs of the MIR community, based on three core design tenets: simplicity, structure, and sustainability. 
This is achieved by combining the advantages of lab-files with lessons learned from previously proposed formats.
The resulting JAMS files are human-readable, easy to drop into existing workflows, and provide solutions to the research trends outlined previously.
We further address classical barriers to adoption by providing tools for easy use with Python and MATLAB, and by offering an array of popular datasets as JAMS files online.
The remainder of this paper is organized as follows: Section \ref{sec:design} identifies three valuable components of an annotation format by considering prior technologies; Section \ref{sec:spec} formally introduces JAMS, detailing how it meets these design criteria and describing the proposed specification by example; Section \ref{sec:using_jams} addresses practical issues and concerns in an informal FAQ-style, touching on usage tools, provided datasets, and some practical shortcomings; and lastly, we close with a discussion of next steps and perspectives for the future in Section \ref{sec:conclusion}.


\section{Core Design Principles}
\label{sec:design}

%Unlike RDF, which places an emphasis on graphs and relationships, JSON is hierarchical, and furthermore was designed with two specific goals in mind: maximal readability and maximal efficiency.
%Other structured data formats, like YAML (YAML is A Markup Language), are perhaps slightly more readable, but the speed with which data can be serialized is unparalleled.
%It is no wonder then that the entirety of the Web Development community has embraced JSON, full stop, and nearly every programming language has its share of JSON libraries and support.

In order to craft an annotation format that might serve the community into the foreseeable future, it is worthwhile to consolidate the lessons learned from both the relative success of lab-files and the challenges faced by alternative formats into a set of principles that might guide our design.
With this in mind, we offer that usability, and thus the likelihood of adoption, is a function of three criteria:

\subsection{Simplicity}
\label{subsec:simplicity}
The value of simplicity is demonstrated by lab-files in two specific ways.
First, the contents are represented in a format that is intuitive, such that the document model clearly matches the data structure and is human-readable, i.e. uses a lightweight syntax.
This is a particular criticism of RDF and XML, which can be verbose compared to plain text.
Second, lab-files are conceptually easy to incorporate into research workflows.
The choice of an alternative file format can be a significant hurdle if it is not widely supported, as is the case with RDF, or the data model of the document does not match the data model of the programming language, as with XML. 

\subsection{Structure}
\label{subsec:structure}
It is important to recognize that lab-files developed as a way to serialize tabular data (i.e. arrays) in a language-independent manner.
Though lab-files excel at this particular use case, they lack the structure required to encode complex data such as hierarchies or mix different data types, such as scalars, strings, multidimensional arrays, etc.
This is a known limitation, and the community has devised a variety of ad hoc strategies to cope with it: folder trees and naming conventions, such as ``\{X\}/\{Y\}/\{Z\}.lab'', where X, Y, and Z correspond to ``artist'', ``album'', and ``title'', respectively\footnote{\url{http://www.isophonics.net/content/reference-annotations}}; parsing rules, such as ``lines beginning with `\#' are to be ignored as comments''; auxiliary websites or articles, decoupled from the annotations themselves, to provide critical information such as syntax, conventions, or methodology.
Alternative representations are able to manage more complex data via standardized markup and named entities, such as fields in the case of RDF or JSON, or IDs, attributes and tags for XML.


\subsection{Sustainability}
\label{subsec:sustainability}

% Annotations are self-sufficient
% There are tools and technologies bigger than us that are maintained by larger communities
Recently in MIR, a more concerted effort has been made toward sustainable research methods, which we see positively impacting annotations in two ways.
First, there is considerable value to encoding methodology and metadata directly in an annotation, as doing so makes it easier to both support and maintain the annotation while also enabling direct analyses of this additional information.
Additionally, it is unnecessary for the MIR community to develop every tool and utility ourselves; we should instead leverage well-supported technologies from larger communities when possible.


% Goals of sustainable research
% 1. Encode methodology and metadata directly in the annotation
% Easier to version control, easier to support, easier to maintain
% 2. We don't need to develop all of our technologies in house, 

\section{Introducing JAMS}
\label{sec:spec}

\begin{figure}[!ht]
 \centerline{
 \includegraphics[width=3.0in]{figs/jams_markup7.pdf}}
 \caption{Diagram illustrating the structure of the JAMS specification.}
 \label{fig:jams_structure}
\end{figure}
So far, we have identified several goals for a music annotation format: a data structure that matches the document model; a lightweight markup syntax; support for multiple annotations, multiple tasks, and rich metadata; easy work-flow integration; cross-language compliance; and the use of pre-existing technologies for stability.
To find our answer, we need only to look to the web development community, who have already identified a technology that meets these requirements.
JavaScript Object Notation (JSON)\footnote{\url{http://www.json.org/}} has emerged as \emph{the} serialization format of the Internet, now finding native support in almost every modern programming language. 
Notably, it was designed to be maximally efficient and human readable, and is capable of representing complex data structures with little overhead.

JSON is, however, only a syntax, and it is necessary to define formal standards outlining how it should be used for a given purpose. 
To this end, we define a specification on top of JSON (JAMS), tailored to the needs of MIR researchers.

\subsection{A Walk-through Example}

Perhaps the clearest way to introduce the JAMS specification is by example. 
Figure \ref{fig:jams_structure} provides the contents of a hypothetical JAMS file, consisting of nearly valid\footnote{The sole exception is the use of ellipses (``$...$'') as continuation characters, indicating that more information could be included.} JSON syntax and color-coded by concept.
JSON syntax will be familiar to those with a background in C-style languages, as it uses square brackets (``$[~]$'') to denote arrays (alternatively, lists or vectors), and curly brackets (``$\{~\}$'') to denote objects (alternatively, dictionaries, structs, or hash maps). 
Defining some further conventions for the purpose of illustration, we use single quotes to indicate field names, italics when referring to concepts, and consistent colors for the same data structures.
Using this diagram, we will now step through the hierarchy, referring back to relevant components as concepts are introduced.

\subsubsection{The JAMS Object}
\label{sec:spec:annotation}
A JAMS file consists of one top-level object, indicated by the outermost bounding box.
This is the primary container for all information corresponding to a music signal, consisting of several task-array pairs, an object for \texttt{file\_metadata}, and an object for \texttt{sandbox}.
A task-array is a list of \emph{annotations} corresponding to a given task name, and may contain zero, one, or many annotations for that task. %, e.g. \texttt{tag} points to one array, \texttt{chord} to another, and so on.
The format of each array is specific to the kind of annotations it will contain; we will address this in more detail in Section \ref{sec:spec:annotation}.

The \texttt{file\_metadata} object (K) is a dictionary containing basic information about the music signal, or file, that was annotated. 
In addition to the fields given in the diagram, we also include an unconstrained \texttt{identifiers} object (L), for storing unique identifiers in various namespaces, such as the EchoNest or YouTube.
Note that we purposely do not store information about the recording's audio encoding, as a JAMS file is format-agnostic.
In other words, we assume that any sample rate or perceptual codec conversions will have no effect on the annotation, within a practical tolerance.

Lastly, the JAMS object also contains a \texttt{sandbox}, an unconstrained object to be used as needed.
In this way, the specification carves out such space for any unforeseen or otherwise relevant data; however, as the name implies, no guarantee is made as to the existence or consistency of this information.
We do this in the hope that the specification will not be unnecessarily restrictive, and that commonly ``sandboxed'' information might become part of the specification in the future.

\subsubsection{Annotations}
\label{sec:spec:annotation}

An \textit{annotation} (B) consists of all the information that is provided by a single annotator about a single task for a single music signal.
Independent of the task, an annotation comprises three sub-components: an array of objects for \texttt{data} (C), an \texttt{annotation\_metadata} object (E), and an annotation-level \texttt{sandbox}.
For clarity, a task-array (A) may contain multiple annotations (B).

Importantly, a \texttt{data} array contains the primary annotation information, such as its chord sequence, beat locations, etc., and is the information that would normally be stored in a lab-file.
Though all \texttt{data} containers are functionally equivalent, each may consist of only one object type, specific to the given task.
Considering the different types of musical attributes annotated for MIR research, we divide them into four fundamental categories:
\begin{enumerate}
  \item Attributes that exist as a single \textit{observation} for the entire music signal, e.g.~tags.
  \item Attributes that consist of sparse \textit{events} occurring at specific times, e.g.~beats or onsets.
  \item Attributes that span a certain time \textit{range}, such as chords or sections.
  \item Attributes that comprise a dense \textit{time series}, such as discrete-time fundamental frequency values for melody extraction.
\end{enumerate}

\noindent These four types form the most atomic data structures, and will be revisited in greater detail in Section \ref{sec:spec:datatypes}. 
The important takeaway here, however, is that data arrays are not allowed to mix fundamental types.

Following \cite{peeters:AnnotatedCorpora:ISMIR:12}, an \texttt{annotation\_metadata} object is defined to encode information about what has been annotated, who created the annotations, with what tools, etc. Specifically,
\textit{corpus} provides the name of the dataset to which the annotation belongs;
\textit{version} tracks the version of this particular annotation;
\textit{annotation\_rules} describes the protocol followed during the annotation process; 
\textit{annotation\_tools} describes the tools used to create the annotation;
\textit{validation} specifies to what extent the annotation was verified and is reliable; 
\textit{data\_source} details how the annotation was obtained, such as manual annotations, online aggregation, game with a purpose, etc.;
\textit{curator} (F) is itself an object with two subfields, \textit{name} and \textit{email}, for the contact person responsible for the annotation;
and \textit{annotator} (G) is another unconstrained object, which is intended to capture information about the source of the annotation. 
While complete metadata are strongly encouraged in practice, currently only \textit{version} and \textit{curator} are mandatory in the specification.



\subsubsection{Datatypes}
\label{sec:spec:datatypes}
Having progressed through the JAMS hierarchy, we now introduce the four atomic data structures, out of which an annotation can be constructed: \textit{observation}, \textit{event}, \textit{range} and \textit{time series}.
For clarity, the \textit{data} array (A) of a \texttt{tag} annotation is a list of \textit{observation} objects; 
the \textit{data} array of a \texttt{beat} annotation is a list of \textit{event} objects; 
the \textit{data} array of a \texttt{chord} annotation is a list of \textit{range} objects; 
and the \textit{data} array of a \texttt{melody} annotation is a list of \textit{time series} objects.
The current space of supported tasks is provided in Table \ref{tab:coverage}.

Of the four types, an \textit{observation} (D) is the most atomic, and used to construct the other three. 
It is an object that has one primary field, \texttt{value}, and two optional fields, \texttt{confidence} and \texttt{secondary\_value}. 
The \texttt{value} and \texttt{secondary\_value} fields may take any simple primitive, such as a string, numerical value, or boolean, whereas the \texttt{confidence} field stores a numerical confidence estimate for the observation. 
A secondary value field is provided for flexibility in the event that an observation requires an additional level of specificity, as is the case in hierarchical segmentation \cite{smith2011design}. 

An \textit{event} (H) is useful for representing musical attributes that occur at sparse moments in time, such as beats or onsets.  
It is a container that holds two \textit{observations}, \texttt{time} and \texttt{label}. 
Referring to the first beat annotation in the diagram, the \texttt{value} of \textit{time} is a scalar quantity (\texttt{0.237}), whereas the \texttt{value} of \texttt{label} is a string (\texttt{`1'}), indicating metrical position.

A \textit{range} (I) is useful for representing musical attributes that span an interval of time, such as chords or song segments (e.g.~intro, verse, chorus). 
It is an object that consists of three \textit{observations}: \texttt{start}, \texttt{end}, and \texttt{label}. 

The \textit{time series} (J) atomic type is useful for representing musical attributes that are continuous in nature, such as fundamental frequency over time. 
It is an object composed of four elements: \texttt{value}, \texttt{time}, \texttt{confidence} and \texttt{label}.
The first three fields are arrays of numerical values, while \texttt{label} is an \textit{observation}.

 \begin{table}[t!]
  \begin{center}
  \begin{tabular}{| c | c | c | c |}
   \hline
   \textit{observation} & \textit{event} & \textit{range} & \textit{time series} \\
   \hline
  \texttt{tag}  & \texttt{beat} & \texttt{chord} & \texttt{melody} \\
  \texttt{genre} & \texttt{onset} & \texttt{segment} & \texttt{pitch} \\
  \texttt{mood}  &   & \texttt{key}   & \texttt{pattern} \\
                        &   & \texttt{note}  &  \\
                        &   & \texttt{source}  &  \\
   \hline
  \end{tabular}
 \end{center}
 \caption{Currently supported tasks and types in JAMS.}
   \label{tab:coverage}
 \end{table}

\subsection{The JAMS Schema}
\label{sec:spec:schema}

The description in the previous sections provides a high-level understanding of the proposed specification, but the only way to describe it without ambiguity is through formal representation. 
To accomplish this, we provide a JSON schema\footnote{\url{http://json-schema.org/}}, a specification itself written in JSON that uses a set of reserved keywords to define valid data structures.
In addition to the expected contents of the JSON file, the schema can specify which fields are required, which are optional, and the type of each field (e.g.~numeric, string, boolean, array or object).
A JSON schema is concise, precise, and human readable. 

Having defined a proper JSON schema, an added benefit of JAMS is that a validator can verify whether or not a piece of JSON complies with a given schema. 
In this way, researchers working with JAMS files can easily and confidently test the integrity of a dataset.
There are a number of JSON schema validator implementations freely available online in a variety of languages, including Python, Java, C, JavaScript, Perl, and more.
The JAMS schema is included in the public software repository (cf.~Section \ref{sec:using_jams}), which also provides a static URL to facilitate directly accessing the schema from the web within a workflow.
%and is also accessible via a fixed URL\footnote{{URL omitted for peer review}} to facilitate directly pulling the schema from the web into a workflow.

\section{JAMS in Practice}
\label{sec:using_jams}

While we contend that the use and continued development of JAMS holds great potential for the many reasons outlined previously, we acknowledge that specifications and standards are myriad, and it can be difficult to ascertain the benefits or shortcomings of one's options.
In the interest of encouraging adoption and the larger discussion of standards in the field, we would like to address practical concerns directly.

\subsection{How is this any different than \emph{X}?}

%Being text-based, JAMS should not feel like a drastic departure from traditional lab-files.
The biggest advantage of JAMS is found in its capacity to consistently represent rich information with no additional effort from the parser and minimal markup overhead.
Compared to XML or RDF, JSON parsers are extremely fast, which has contributed in no small part to its widespread adoption.
These efficiency gains are coupled with the fact that JAMS makes it easier to manage large data collections by keeping all annotations for a given song in the same place.

\subsection{What kinds of things can I do with JAMS that I can't already do with \emph{Y}?}
JAMS can enable much richer evaluation by including multiple, possibly conflicting, reference annotations and directly embedding information about an annotation's origin.
A perfect example of this is found in the Rock Corpus Dataset~\cite{de2011corpus}, consisting of annotations by two expert musicians: one, a guitarist, and the other, a pianist.
Sources of disagreement in the transcriptions often stem from differences of opinion resulting from familiarity with their principal instrument, where the voicing of a chord that makes sense on piano is impossible for a guitarist, and vice versa.
Similarly, it is also easier to develop versatile MIR systems that combine information across tasks, as that information is naturally kept together.

Another notable benefit of JAMS is that it can serve as a data representation for algorithm outputs for a variety of tasks.
For example, JAMS could simplify MIREX submissions by keeping all machine predictions for a given team together as a single submission, streamlining evaluations, where the annotation sandbox and annotator metadata can be used to keep track of algorithm parameterizations.
This enables the comparison of many references against many algorithmic outputs, potentially leading to a deeper insight into a system's performance.


\subsection{So how would this interface with my workflow?}
Thanks to the widespread adoption of JSON, the vast majority of languages already offer native JSON support.
In most cases, this means it is possible to go from a JSON file to a programmatic data structure in your language of choice in a single line of code using tools you didn't have to write.
To make this experience even simpler, we additionally provide two software libraries, for Python and MATLAB.
In both instances, a lightweight software wrapper is provided to enable a seamless experience with JAMS, allowing IDEs and interpreters to make use of autocomplete and syntax checking.
Notably, this allows us to provide convenience functionality for creating, populating, and saving JAMS objects, for which examples and sample code are provided with the software library\footnote{\url{https://github.com/urinieto/jams}}.


\subsection{What datasets are already JAMS-compliant?}
To further lower the barrier to entry and simplify the process of integrating JAMS into a pre-existing workflow, we have collected some of the more popular datasets in the community and converted them to the JAMS format, linked via the public repository. 
The following is a partial list of converted datasets: Isophonics (beat, chord, key, segment); Billboard (chord); SALAMI (segment, pattern); RockCorpus (chord, key); tmc323 (chords); Cal500 (tag); Cal10k (tag); ADC04 (melody); and MIREX05 (melody).

\subsection{Okay, but \emph{my} data is in a different format -- now what?}
We realize that it is impractical to convert every dataset to JAMS, and provide a collection of Python scripts that can be used to convert lab-files to JAMS. 
In lieu of direct interfaces, alternative formats can first be converted to lab-files and translated to JAMS thusly.

\subsection{My MIR task doesn't really fit with JAMS.}
That's not a question, but it is a valid point and one worth discussing.
While this first iteration of JAMS was designed to be maximally useful across a variety of tasks, there are two broad reasons why JAMS might not work for a given dataset or task.
One, a JAMS annotation only considers information at the temporal granularity of a single audio file and smaller, independently of all other audio files in the world. 
Therefore, extrinsic relationships, such as cover songs or music similarity, won't directly map to the specification because the concept is out of scope.

The other, more interesting, scenario is that a given use case requires functionality we didn't plan for and, as a result, JAMS doesn't yet support.
To be perfectly clear, the proposed specification is exactly that --a proposal-- and one under active development.
Born out of an internal need, this initial release focuses on tasks with which the authors are familiar, and we realize the difficulty in solving a global problem in a single iteration.
As will be discussed in greater detail in the final section, the next phase on our roadmap is to solicit feedback and input from the community at large to assess and improve upon the specification.
If you run into an issue, we would love to hear about your experience.

% If not JAMS, for the love of god use JSON.

\subsection{This sounds promising, but nothing's perfect. There must be shortcomings.}
Indeed, there are two practical limits that should be mentioned.
Firstly, JAMS is not designed for features or signal level statistics.
That said, JSON is still a fantastic, cross-language syntax for serializing data, and may further serve a given workflow.
As for practical concerns, it is a known issue that parsing large JSON objects can be slow in MATLAB.
We've worked to make this no worse than reading current lab-files, but speed and efficiency are not touted benefits of MATLAB.
This may become a bigger issue as JAMS files become more complete over time, but we are actively exploring various engineering solutions to address this concern.

\section{Discussion and Future Perspectives}
\label{sec:conclusion}

In this paper, we have proposed a JSON format for music annotations to address the evolving needs of the MIR community by keeping multiple annotations for multiple tasks alongside rich metadata in the same file.
We do so in the hopes that the community can begin to easily leverage this depth of information, and take advantage of ubiquitous serialization technology (JSON) in a consistent manner across MIR.
The format is designed to be intuitive and easy to integrate into existing workflows, and we provide software libraries and pre-converted datasets to lower barriers to entry.

Beyond practical considerations, JAMS has potential to transform the way researchers approach and use music annotations.
One of the more pressing issues facing the community at present is that of dataset curation and access.
It is our hope that by associating multiple annotations for multiple tasks to an audio signal with retraceable metadata, such as identifiers or URLs, it might be easier to create freely available datasets with better coverage across tasks.
Annotation tools could serve music content found freely on the Internet and upload this information to a common repository, ideally becoming something like a Freebase\footnote{http://www.freebase.com} for MIR.
Furthermore, JAMS provides a mechanism to handle multiple concurrent perspectives, rather than forcing the notion of an objective truth.

%over time compiling a common repository of JAMS.
%Additionally, We have already integrated the format with an open source MIR evaluation library\footnote{URL omitted for peer review}, and the developers of SonicVisualizer have expressed interest in integrating JAMS in the future.

Finally, we recognize that any specification proposal is incomplete without an honest discussion of feasibility and adoption.
The fact remains that JAMS arose from the combination of needs within our group and an observation of wider applicability. 
We have endeavored to make the specification maximally useful with minimal overhead, but appreciate that community standards require iteration and feedback.
This current version is not intended to be the definitive answer, but rather a starting point from which the community can work toward a solution as a collective.
Other professional communities, such as the IEEE, convene to discuss standards, and perhaps a similar process could become part of the ISMIR tradition as we continue to embrace the pursuit of reproducible research practices.



%Looking forward in more concrete terms, we place a high priority on identifying strategies to read and write JAMS files faster as they grow, potentially indefinitely, in size.
%This is certainly not a concern any time in the near future, but one worth preparing for in the interim.
%There are existing technologies, such as streaming APIs\footnote{http://lloyd.github.io/yajl/}, BSON\footnote{http://bsonspec.org/} or MessagePack\footnote{http://msgpack.org/}, which may help alleviate this immediately.
%Fortunately, this is a known issue in the larger world of software development and data serialization, and it is unlikely MIR researchers will experience this issue before a solution exists.




% PLEASE LEAVE THIS CODE HERE FOR REFERENCE
% \begin{table}
%  \begin{center}
%  \begin{tabular}{|l|l|}
%   \hline
%   String value & Numeric value \\
%   \hline
%   Hello ISMIR  & 2014 \\
%   \hline
%  \end{tabular}
% \end{center}
%  \caption{Table captions should be placed below the table.}
%  \label{tab:example}
% \end{table}
% 
% \begin{figure}
%  \centerline{\framebox{
%  \includegraphics[width=\columnwidth]{figure.png}}}
%  \caption{Figure captions should be placed below the figure.}
%  \label{fig:example}
% \end{figure}
% 
% \section{Equations}
% 
% Equations should be placed on separated lines and numbered.
% The number should be on the right side, in parentheses.
% 
% \begin{equation}
% E=mc^{2}
% \end{equation}

\bibliography{jams_bibliography}

\end{document}
