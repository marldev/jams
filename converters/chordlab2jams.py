import argparse

from jams.converters import utils
from jams.pyjams import save, JAMS


def chord_lab_to_jams(input_file, output_file, metadata_file=None):
    intervals, labels = utils.three_col_lab_to_data(input_file)
    jam = JAMS()
    utils.populate_range_annotation(
        intervals, labels, jam.chord.create_annotation())
    save(jam, output_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="")

    parser.add_argument("input_file",
                        metavar="input_file", type=str,
                        help="")
    parser.add_argument("output_file",
                        metavar="output_file", type=str,
                        help="")
    parser.add_argument("--metadata_file",
                        metavar="--metadata_file", type=str,
                        help="")
    args = parser.parse_args()
    chord_lab_to_jams(args.input_file, args.output_file)
