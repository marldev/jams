"""
All converters should adhere to the same conventions / syntax / interface.

xlab2jams.py
    If the output file exists, append; if it doesn't, create.

    Parameters
    ----------
    input_labfile: str
    output_jamfile: str
    file_metadata: str
    annotation_metadata: str
    on_metadata_conflict: str, default='fail'

    Returns
    -------
    Joy & happiness.
"""
from . import beatlab2jams
from . import chordlab2jams
from . import keylab2jams
from . import segmentlab2jams
from . import utils
