"""JAMS Python API

This library provides an interface for reading JAMS into Python, or creating
them programatically.


1. Creating a JAM from scratch
------------------------------
First, create a top-level Jams object for a given attribute. Here, we'll create
a beat annotation:

  >>> from jams import Jams
  >>> annot = Jams(attribute='beats')

Then, we'll update the annotation's metadata by directly setting its fields:

  >>> annot.annotation_metadata.origin = "Earth"
  >>> annot.annotation_metadata.annotator.email = "grandma@aol.com"


That's not all that interesting though. Now we can populate the annotation:

  >>> beat = annot.create_datapoint()
  >>> beat.time.value = 0.33
  >>> beat.time.confidence = 1.0
  >>> beat.label.value = "1"
  >>> beat.label.confidence = 0.75


And now a second time, cause this is our house (and we can do what we want):

  >>> beat2 = annot.create_datapoint()
  >>> beat2.label.value = "second beat"


Once you've added all your data, you can serialize the annotation to a file
with the built-in `json` library:

  >>> import json
  >>> with open("these_are_my.jams", 'w') as f:
          json.dump(annot, f, indent=2)

Or, less verbosely, using the built-in save function:

  >>> import jams
  >>> jams.save("these_are_my.jams", annot)


2. Reading a Jams file
----------------------
Assuming you already have a JAMS file on-disk, say at 'these_are_my.jams', you
can easily read it back into memory:

  >>> jam2 = J.load('these_are_my.jams')


And that's it!

  >>> print jam2.to_string()

"""

__VERSION__ = '0.0.0'

from . import converters
# from fileio import load
# from fileio import dump
# from fileio import append
# from pyjams import Jams
