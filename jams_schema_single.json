{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "id": "#root",

    "definitions": {

        "file_metadata": {
            "type": "object",
            "title": "File metadata",
            "properties": {
                "echonest_id": { "type": "string", "pattern": "[A-Z]+[0-9]+"},
                "musicbrainz_id": { "type": "string"},
                "artist": { "type": "string"},
                "title": { "type": "string"},
                "album": { "type": "string"},
                "duration": { "type": "string", "pattern": "[0-9][0-9]:[0-9][0-9]:[0-9][0-9]"}
            }
        },

        "annotation_metadata": {
            "type": "object",
            "title": "Annotation metadata",
            "description": "All metadata required to describe an annotation",
            "properties": {
                "attribute": { "type": "string" },
                "corpus": { "type": "string" },
                "version": { "type": "string" },
                "annotator": {
                    "type": "object",
                    "title": "Annotator",
                    "description": "Annotator name and email",
                    "properties": {
                        "name": { "type": "string"},
                        "email": { "type": "string", "format": "email" }
                    }
                },
                "annotation_tools": { "type": "string"},
                "annotation_rules": { "type": "string"},
                "validation_and_reliability": { "type": "string"},
                "origin": { "type": "string"}
            },
            "required": ["attribute", "version", "annotator"]
        },

        "observation": {
            "type": "object",
            "title": "Observation",
            "description": "An observation with a single value and confidence, e.g. a time, a frequency value, or a chord label",
            "properties": {
                "value": { "type": ["number","string","boolean"] },
                "confidence": { "type": "number" },
                "secondary_value": { "type": ["number","string","boolean"] }
            },
            "required": ["value"]
        },

        "event": {
            "type": "object",
            "title": "Event",
            "description": "An event that occurs in a single point in time, e.g. an onset",
            "properties": {
                "time": { "$ref": "#/definitions/observation" },
                "label": { "$ref": "#/definitions/observation" }
            },
            "required": ["time"]
        },

        "range": {
            "type": "object",
            "title": "Range",
            "description": "A concept that spans a certain time range, e.g. a chord",
            "properties": {
                "start": { "$ref": "#/definitions/observation" },
                "end": { "$ref": "#/definitions/observation" },
                "label": { "$ref": "#/definitions/observation" }
            },
            "required": ["start","end"]
        },

        "time_series": {
            "type": "object",
            "title": "Time Series",
            "description": "A sequence of values with corresponding timestamps and confidence esitmates",
            "properties": {
                "label": { "type": "string" },
                "value": {
                    "type": "array",
                    "title": "Value",
                    "description": "The values of the time series",
                    "items": { "type": ["number","string","boolean"] }
                },
                "time": {
                    "type": "array",
                    "title": "Time",
                    "description": "The timestamps of the time series",
                    "items": { "type": "number" }
                },
                "confidence": {
                    "type": "array",
                    "title": "Confidence",
                    "description": "The per-value confidence of the time series",
                    "items": { "type": "number" }
                }
            },
            "required": ["value", "time"]
        }
    },

    "type": "object",
    "title": "JAMS file",
    "description": "JSON Annotated Music Specification",
    "properties": {

        "observation_annotation": {
            "id": "#observation-annotation",
            "type": "object",
            "title": "Observation Annotation",
            "description": "An annotation of a single observation, e.g. tags",
            "properties": {
                "annotation_type" : { "type": "string" },
                "annotation_metadata": { "$ref": "#/definitions/annotation_metadata" },
                "file_metadata": { "$ref": "#/definitions/file_metadata" },
                "data": {
                    "type": "array",
                    "title": "Data",
                    "description": "An array of observations",
                    "items": { "$ref": "#/definitions/observation" }
                },
                "sandbox": { "type": "object"}
            },
            "required": ["annotation_type","annotation_metadata","file_metadata","data"]
        },

        "event_annotation" : {
            "id": "#event-annotation",
            "type": "object",
            "title": "Event Annotation",
            "description": "An annotation of events, e.g. beats, onsets",
            "properties": {
                "annotation_type" : { "type": "string" },
                "annotation_metadata": { "$ref": "#/definitions/annotation_metadata" },
                "file_metadata": { "$ref": "#/definitions/file_metadata" },
                "data": {
                    "type": "array",
                    "title": "Data",
                    "description": "An array of events",
                    "items": { "$ref": "#/definitions/event" }
                },
                "sandbox": { "type": "object"}
            },
            "required": ["annotation_type","annotation_metadata","file_metadata","data"]
        },

        "range_annotation" : {
            "id": "#range-annotation",
            "type": "object",
            "title": "Range Annotation",
            "description": "An annotation of ranges, e.g. chords, sections",
            "properties": {
                "annotation_type" : { "type": "string" },
                "annotation_metadata": { "$ref": "#/definitions/annotation_metadata" },
                "file_metadata": { "$ref": "#/definitions/file_metadata" },
                "data": {
                    "type": "array",
                    "title": "Data",
                    "description": "An array of ranges",
                    "items": { "$ref": "#/definitions/range" }
                },
                "sandbox": { "type": "object"}
            },
            "required": ["annotation_type","annotation_metadata","file_metadata","data"]
        },

        "time_series_annotation" : {
            "id": "#time-series-annotation",
            "type": "object",
            "title": "Time Series Annotation",
            "description": "An annotation of a time series, e.g. melody (f0)",
            "properties": {
                "annotation_type" : { "type": "string" },
                "annotation_metadata": { "$ref": "#/definitions/annotation_metadata" },
                "file_metadata": { "$ref": "#/definitions/file_metadata" },
                "data": {
                    "type": "array",
                    "title": "Data",
                    "description": "An array of time series",
                    "items": { "$ref": "#/definitions/time_series" }
                },
                "sandbox": { "type": "object"}
            },
            "required": ["annotation_type","annotation_metadata","file_metadata","data"]
        }
    }
}
